
<?php
//Pour que les taxes soiyent initialisés.
define("TPS",0.05);
define("TVQ",0.09975);

  function ChargeurDeClasseAutomatique($class) //Charge les classes avec un autoload.
  {
    require_once "Classes/$class.php";
	
  }
  spl_autoload_register('ChargeurDeClasseAutomatique');  

function GrandeurFenetre()
{
echo '<table width = "100%" height = "150%">';
}

function AfficheTitre()//Affiche l'entête avec le titre et une image.
{
	echo'
	<tr>
		<td id="entete" height="225" colspan="3"> 
			<h1>SHIZUKI <div id="auto">AUTO</div></h1>
			<a id="boutonPanier" href="panier.php?quoiFaire="afficher">Mon panier</a>
			<a id="boutonDossier" href="authentification.php?prov=dossier">Mon dossier</a>	
			<a id="boutonCommander" href="commande.php">Commander</a>';
			if(isset($_SESSION['authentification']))
			{
				echo'<p id="connection"> Connecté :';
				echo $_SESSION['authentification'];
				echo'</p>';
			}
		echo'</td>
	</tr>';
	//Ajout d'un bouton qui va directement au panier.
}

function AfficheTitreAdmin()//Affiche l'entête avec le titre et une image.
{
	echo'
	<tr>
		<td id="entete" height="225" colspan="3"> 
			<h1>SHIZUKI <div id="auto">AUTO</div></h1>';
			if(isset($_SESSION['authentification']))
			{
				echo"<p id='connection'> Connecté :";
				echo $_SESSION['authentification'];
				echo'</p>';
			}
		echo'</td>
	</tr>';
	//Ajout d'un bouton qui va directement au panier.
}

function ColonneBas()//Affiche la dernière colonne en bas avec une image.
{
	echo '
	<tr>
		<td id="bas" height="150" colspan="3"></td>
	</tr>
</table>';
}

function ColonneGauche()//Affiche la colonne de gauche avec un bouton pour revenir au catalogue principal.
{

	echo'
	<tr id="coGauche">
		<td id="Gauche" width = "200">
			<a id="boutonCatalogue" href="index.php">Notre catalogue</a> 
			<a id="boutonPolitique" href="politique.php">Nos politiques</a>';
			if(!isset($_SESSION['authentification']))
			{
				echo '<a id="boutonAuthen" href="authentification.php">Authentification (login)</a>';
			}
			if(isset($_SESSION['authentification']))
			{
				echo '<a id="boutonFermer" href="deconnection.php">Fermer compte</a>';
			}
			if(isset($_SESSION['authentification']))
			{
				echo '<a id="boutonMotPasse" href="motPasse.php">Changer mot de passe</a>';
			}
			if(isset($_SESSION['authentification']))
			{
				echo '<a id="boutonPassees" href="commandesPassees.php">Commandes passées</a>';
			}
			
		echo'</td>';
}

function ColonneGaucheAdmin()//Affiche la colonne de gauche avec un bouton pour revenir au catalogue principal.
{

	echo'
	<tr id="coGauche">
		<td id="Gauche" width = "200">
			<a id="boutonCatalogue" href="index.php">Accueil</a> ';
			
			if(isset($_SESSION['authentification']))
			{
				echo '<a id="boutonCate" href="authentification.php">Catégories</a>';
			}
			if(isset($_SESSION['authentification']))
			{
				echo '<a id="boutonFermer" href="deconnection.php">Films</a>';
			}
			if(isset($_SESSION['authentification']))
			{
				echo '<a id="boutonMotPasse" href="motPasse.php">Rapports</a>';
			}
			if(isset($_SESSION['authentification']))
			{
				echo '<a id="boutonPassees" href="../deconnection.php">Quitter</a>';
			}
			
		echo'</td>';
}


function ColonneDroite()//Affiche la colonne de droite avec la fonction de recherche pour les autos.
{
	echo'
		<div>
			<td id="Droite" width = "200">';

	echo '<div id="recherche">';
	echo 'Recherche : ';
	
	try {$maBD = new BDService();}catch(Exeption $e) {	echo "$e->getmessage()";}
	
	$req = "SELECT description_categorie FROM categories"; //Prend les informations des sortes de catégories.

	try {	$resultat = $maBD->Select($req);}catch(Exeption $e) {echo "$e->getmessage()";}
	//si le nom est grave
	echo "
		<form action='index.php' method='get'>
			<select name='listeCategories'>
				<option value='0'>Toutes</option>";
			//Ajoute une catégorie TOUTES.
			
	$iCompteur=0;
	//Affiche les catégories et créer le bouton pour envoyer l'information.
	foreach ($resultat as $ligne) {
		$iCompteur++;
		$categorie = $ligne['description_categorie'];
		echo "
				<option value='$iCompteur'>$categorie</option>";
    }
	
	echo "
			</select><br>
			<input type='text' name='recherche'/><br>
			<input type='submit' value='Chercher'/>
		</form>
		</td>
		</div>
		</div>";
}

?>