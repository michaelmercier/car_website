﻿<html>
<head>
	<meta charset='iso-8859-1'>
	<link href="CSS/style.css" rel="stylesheet" type="text/css">
</head>
<?php
require_once "Biblio/FonctionCommune.php";
 
 session_start();//Partir la session qui va avoir les objects dans le panier èa vendre.
 
 $panier = new PanierAchats();
 
 //Ouvre les fonctions du fichier fonctionCommune pour afficher le site.
    GrandeurFenetre();
	AfficheTitre();
	ColonneGauche();
	
	//Selon qu'est ce que doit faire le panier.
  if (isset($_GET['quoiFaire'])) {
	switch ($_GET['quoiFaire']) {
	case "ajout": $panier->ajouter($_GET['noProduit']); //Ajouter un produit au panier.
		break;
	case "modification": $panier->modifier(); //Modifier le nombre de produit dans le panier pour le même produit.
		break;
	case "suppression": $panier->supprimer($_GET['num']);//Supprimer un produit dans le panier.
		break;
	case "vider": session_destroy(); //Detruire tout le panier.
		break;
	default : break;
	} // Fin du switch
		header("location:panier.php");
		exit();
  } // Fin du if 
  
    $panier->afficher();//On affiche le panier.
	ColonneBas();
	
	
	
?>
</html>