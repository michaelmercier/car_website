﻿<html>
<head>
	<meta content="text/html; charset=iso-8859-1" />
	<link href="CSS/style.css" rel="stylesheet" type="text/css">
</head>
 <body>
 <?php

 require_once "Biblio/FonctionCommune.php";
  session_start();
  
 //Affiche les standartds du site web.
    GrandeurFenetre();
	AfficheTitre();
	ColonneGauche();
	
	
	if (isset ( $_POST['valider'])) { //On a cliqué sur le bouton valider.
		$tableau = array(); //On transfère dans le tableau tout le contenu du POST.
		foreach ($_POST as $cle => $valeur)
		{
			$tableau[$cle]= htmlentities($valeur); //htmlentities pour la protection des informations.
		}
		$client = new Client ($tableau); //Créer un nouveau client.
		
	}
	//Si ce n'est pas valider mais l'authentification parce que le client a un dossier mais ne le modifie pas.
	else if (isset($_SESSION['client'])) { //Il y a déjà un contenu dans la variable de SESSION
		$client = new Client ($_SESSION['client']);
	}
	else
	$client = new Client (array());

	// Pour un nouveau dossier.
	$tClient = $client->getClient();
		
	$nbErreurs = EcrireTableau($tClient);
		
		//Si valider et qu'il n'y a pas d'erreur.
	if(isset($_POST['valider']) && $nbErreurs==0)
	{		
		$client->setClient($tClient); 
		$_SESSION['client']=$tClient; //La session devient le client.
		echo "<meta http-equiv='Refresh' content='0;url=confirmation.php' />";
	}
	
	
	ColonneBas();

	
	function EcrireTableau($tClient) //Pour écrire le tableau principal d'inscription et pour afficher les erreurs.
	{
		$classe = "blanc"; //Notre classe est blanc sans erreur au début.
		
		$infos = array(	array("Nom :", "Prénom :", "Courriel :", // Tableau à 4 dimension.
							  "Adresse :", "Ville :", "Code postal :", //Affiche les champs qu'on veut remplire.
							  "Téléphone :", "Nom d'usager :", "Mot de passe :",
							  "Confirmation du mot de passe :"),
						array("Nom(min. 3 carac. valables) :",  //Le tableau s'il y a des erreurs.
							  "Prénom(min. 3 carac. valables) :",
							  "Courriel(modèle x@y.z) :",
							  "Adresse(min. 3 car. valables) :", //label avec erreur
							  "Ville(min. 3 car. valables) :", 
							  "Code postal (sans espace)(modèle A9A9A9) :", 
							  "Téléphone(modèle (xxx)yyy-zzzz) :", 
							  "Nom d'usager(min. 5 car. valables) :",
							  "Mot de passe(min. 5 car. valables) :",
							  "Confirmation du mot de passe(min. 5 car. valables) :"),
						array('nom', 'prenom', 'courriel', 'numerociviqueetrue', 'ville', 'codepostal', 'telephone', 'usager', 'motPasse', 'confirm'), //Les noms dans la BD.
						array("20", "20", "40", "50", "35", "6", "14", "15", "15", "15"),	//Grosseur des textbox.
					);
		$nbErreurs=0; //Pas d'erreur pour commencer.
		//Commence à afficher l'interface.
		echo"
			<td colspan='2'>
				<form action='inscription.php' method='post'>
					<table align='center' id='inscription'>";
		
		if (isset ($_GET['message']) && $_GET['message']=="doublon") //S'il y a un message d'erreur ou un message de doublon.
		
			echo "<span class='rouge'>Ce compte existe déjà!</span>"; //La classe est rouge parce que le compte existe déjà.
			
			echo"
				<tr>
					<td>
						<b>Formulaire de saisie</b>
					</td>
				</tr>
				<tr>
					<td>
						<b>Genre :</b>
					</td>
					<td>
						<input type='radio' value='F' name='genre' id='bGenre' />
						<b>Mme</b><br/>
						<input type='radio' value='M' name='genre' checked/>
						<b>M<br/></b>
					</td>
				</tr>";
	
		for($i=0;$i<sizeof($infos[0]);$i++) {		//Pour le nombre de champs qu'on doit afficher à l'écran.
			$champs='';
			$type='text';
			$posLabel=0;//Pour commencer avec le premier champ.
			
			if($i==6)//Si le champ est 6(province) on affiche la province.
			{
				afficherProvince($tClient['province']);
			}
			if(isset($_SESSION['authentification']) && $i==7) //Si l'utilisateur est déjà connecté et que le champ est à 7(Nom d'usager) on le masque.
			{
				$type='hidden';
			}
			if($i==8||$i==9) //Quand on arrive au champ 8 et 9(mot de passe) le type devient password pour plus de protection.
			{
				$type='password';
			}

			if (isset($_POST['valider'])) //Si le post est valider.
			{
				if($i==9) //Et que le champs est à 9(mot de passe confirmation)
				{
					if(isset($_POST['motPasse'])) // Si le mot de passe ne correspond pas la classe devient rouge pour dire qu'il y a un erreur.
						if($_POST['motPasse']!=$_POST['confirm'])
						{
							$classe='rouge';
						}
				}
				else {
					if(isset($tClient[$infos[2][$i]])) 
					{
						$classe=validerChamp($infos[2][$i]); //Valide l'informations du client.
						if($i!=8)
						{
							$champs=$tClient[$infos[2][$i]];
						}
					}
				}
				if($classe=='rouge') //Si la classe est rouge on ajoute u erreur.
				{
					$posLabel=1;
					$nbErreurs++;
				}
			}
			
			if($i==7 && isset($_SESSION['authentification']))//Si l'utilisateur est déjà connecté et que le champ est à 7(nom d'usager) on l'affiche mais pas dans un textbox.
				{
					echo "<tr><td class='",$classe,"'><b>",$infos[$posLabel][$i],"</b></td>
					<td>".$_SESSION['authentification']."</td></tr>";
				}
			
			if(isset($_SESSION['authentification']))//Si l'utilisateur est connecté.
			{	//Prend l'information dans la BD pour pouvoir l'afficher dans les textbox.
				try {$bd = new BDService;} catch(Exception $e){echo $e->getMessage();}
				$sel = "select usager,nom,prenom,numerociviqueetrue,ville,codepostal,courriel,telephone,province,genre from Clients where usager = '".$_SESSION['authentification']."'";
				try {$res =  $bd->Select($sel);} catch(Exception $e){echo $e->getMessage();}
			
				if($i!=8 && $i!=9) //Si les champs ne sont pas à 8 et 9(mot de passe). On les affichent.
				{
					$champs = $res[0][$infos[2][$i]];
				}
				
			}
			//Si le client n'est pas connecté et que le champ est à 8 ou à 9.
			if( !(isset($_SESSION['authentification']) && $i==8) && !(isset($_SESSION['authentification']) && $i==9))
			{
				if($type=='hidden')//On le cache si le type est hidden.
				{
					echo "
						<tr>
							<td></td>
							<td>
								<input type='",$type,"' size='",$infos[3][$i],"' name='",$infos[2][$i],"' maxlength='",$infos[3][$i],"' value='",$champs,"'>
							</td>
						</tr>";
				}
				else
				{//Ou on l'affiche.
					echo "
							<tr>
								<td class='",$classe,"'><b>",$infos[$posLabel][$i],"</b></td>
								<td>
									<input type='",$type,"' size='",$infos[3][$i],"' name='",$infos[2][$i],"' maxlength='",$infos[3][$i],"' value='",$champs,"'>
								</td>
							</tr>";
				}
			}
		}
		//Affiche le bouton valider à la fin de tout.
		echo 	"<tr>
				<td>&nbsp;</td>
				<td><input type='submit' name='valider' value='Valider'/></td>
				</tr>
			</table>
			</form>";
			return $nbErreurs; //Retourne le nombre d'erreur.
	}
	
	function afficherProvince($prov) //Affiche les provinces dans un "combobox".
	{
		$provinces=array('QC'=>'Québec', 'ON'=>'Ontario','AB'=>'Alberta','BC'=>'Colombie-Britannique',
		'PE'=>'Ile-du-Prince-Édouard', 'MB'=>'Manitoba', 'NB'=>'Nouveau-Brunswick',
		'NS'=>'Nouvelle-Écosse', 'NU'=>'Nunavut', 'SK'=>'Saskatchewan',
		'NL'=>'Terre-Neuve et Labrador', 'NT'=>'Territoires du Nord-Ouest', 'YU'=>'Yukon');
		echo "<tr><td class='blanc'><b>Province :</b></td><td>\n";
		echo "<select name='province'>\n";
		foreach ($provinces as $cle=> $valeur) //S'il s'agit de la province transmise en paramètre, elle est pré-sélectionnée dans la liste
			echo "<option value='$cle'",($cle==$prov?" selected='selected'":''),">$valeur</option>\n";
		
		echo "</select>\n";
		echo "</td></tr>\n";
	}
	
	function validerChamp($champ) { //Valide les valeurs donné par l'utilisateur.
	
		global $valide;
		global $tClient;
		$classe = 'blanc';
		
		if (! empty($tClient[$champ])) //Si les champs ne sont pas vide.
			$tClient[$champ] = trim($tClient[$champ]);
		
		switch ($champ)
		{
			case 'nom':
			case 'prenom':
			
			$modele="/[A-Z]{1}[a-z- '.]{2,}/"; //Pour nom et prénom 1 majuscule et au moin 3 lettres.
			if(!preg_match($modele, $tClient[$champ]))
			{
				$classe = 'rouge';
				$valide = false;
			}
			break;
			
			case 'numerociviqueetrue':
			case 'ville':
			
			$modele="/[A-Za-z- '.]{3,}/"; // Au moin 3 lettres.
				if (!preg_match($modele, $tClient[$champ])) {
					$classe = 'rouge';
					$valide = false;
				}
				break;
				
			case 'usager';
			case 'motPasse';
			
			$modele="/[A-Za-z0-9]{5,}/"; //Au moin 5 champs.
				if (!preg_match($modele, $tClient[$champ])) {
					$classe = 'rouge';
					$valide = false;
				}
				break;
				
			case 'courriel':
				$modele="/^[a-zA-Z0-9._]+@[a-z0-9._]{1,}\.[a-z]{1,}$/";//EX: abc@ho.ca
				
				if(!preg_match($modele, $tClient[$champ])) {
					$classe = 'rouge';
					$valide = false;
				}
				break;
				
			case 'codepostal':
				$modele="/^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$/"; //Majuscule ou minuscule avec EX : j5k2k9 et champs pas vide.
				
				if(!preg_match($modele, $tClient[$champ])) {
					$classe = 'rouge';
					$valide = false;
				}
				break;
				
			case 'telephone':
				$modele="/^((\([0-9]{3}\))|[0-9]{3})[-\s]?[0-9]{3}[-\s]?[0-9]{4}$/"; //Avec les - et les espaces et les ().
				
				if(!preg_match($modele, $tClient[$champ])) {
					$classe = 'rouge';
					$valide = false;
				}
				break;
		}
		return $classe;
	}
?>
</body>
</html>












