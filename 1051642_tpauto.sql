-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 11 Avril 2013 à 17:49
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `1051642_tpauto`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `idCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `description_categorie` varchar(30) NOT NULL,
  PRIMARY KEY (`idCategorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`idCategorie`, `description_categorie`) VALUES
(1, 'Sportive'),
(2, 'sous-compacte'),
(3, 'Berline');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `idClient` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `numerociviqueetrue` varchar(50) NOT NULL,
  `ville` varchar(30) NOT NULL,
  `codepostal` varchar(6) NOT NULL,
  `province` varchar(2) NOT NULL,
  `courriel` varchar(40) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `genre` varchar(1) NOT NULL,
  PRIMARY KEY (`idClient`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `clients`
--

INSERT INTO `clients` (`idClient`, `nom`, `prenom`, `numerociviqueetrue`, `ville`, `codepostal`, `province`, `courriel`, `telephone`, `genre`) VALUES
(1, 'Yui', 'suzuki', '1716 yokohama', 'Osaka', 'O5GH7K', 'OS', 'yuiyui@hotmail.jp', '4566544587', 'F'),
(2, 'Yoko', 'Aragaki', '136 Edawa', 'Kyoto', 'J7H2K9', 'KT', 'lolita_sweet@hotmail.jp', '4526589874', 'F'),
(3, 'Maeda', 'Honda', '666 Takeda', 'Tokyo', 'H7J8K9', 'TY', 'akb48_TeamA@hotmail.jp', '6589658741', 'F'),
(4, 'Jean', 'Mercier', '7654 Lafleur', 'Montreal', 'J6K8H7', 'QC', 'jeanmercier@hotmail.com', '5146589652', 'M'),
(5, 'Vincent', 'Legare', '69 Esthetiques', 'Sydney', 'J8U9I0', 'SD', 'DoUEventLift@live.au', '4879658745', 'M');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
  `idCommande` int(11) NOT NULL AUTO_INCREMENT,
  `idClient` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`idCommande`),
  KEY `idClient` (`idClient`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `commandes`
--

INSERT INTO `commandes` (`idCommande`, `idClient`, `date`) VALUES
(1, 1, '2013-03-07'),
(2, 1, '2013-03-05'),
(3, 1, '2013-03-04'),
(4, 1, '2013-03-09'),
(5, 2, '2013-03-21'),
(6, 2, '2013-03-28'),
(7, 2, '2013-03-10'),
(8, 3, '2013-03-30'),
(9, 4, '2013-03-07');

-- --------------------------------------------------------

--
-- Structure de la table `commandesdetails`
--

CREATE TABLE IF NOT EXISTS `commandesdetails` (
  `idCommande` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  KEY `idCommande` (`idCommande`),
  KEY `idProduit` (`idProduit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `commandesdetails`
--

INSERT INTO `commandesdetails` (`idCommande`, `idProduit`, `quantite`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 4, 1),
(4, 6, 1),
(5, 8, 1),
(6, 2, 2),
(7, 1, 1),
(8, 10, 3),
(9, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `idProduit` int(11) NOT NULL AUTO_INCREMENT,
  `idCategorie` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prix` int(9) NOT NULL,
  `description_produit` varchar(255) NOT NULL,
  `quantiteProduit` int(11) NOT NULL,
  `quantiteminimal` int(11) NOT NULL,
  PRIMARY KEY (`idProduit`),
  KEY `idCategorie` (`idCategorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`idProduit`, `idCategorie`, `nom`, `prix`, `description_produit`, `quantiteProduit`, `quantiteminimal`) VALUES
(1, 1, 'nissan skyline R32', 8001, 'GT-R - 2,6 litres RB26DETT twin-turbo six cylindres en ligne, 280 ch (206 kW) (versions NISMO, N1, VSPEC et VSPECII) 1989', 5, 1),
(2, 1, 'nissan slivia', 8999, 'S-15 4 cylindres en ligne 1809 cm³ 8 soupapes Turbo 1984', 3, 1),
(3, 2, 'toyota trueno', 12000, 'Trueno 1600 GT APEX (AE86) (moteur 4A-GE 1600 Twin Cam) 1983', 2, 1),
(4, 2, 'toyota supra', 32000, '6 cylindres en ligne 24 soupapes 3 litres atmosphérique (2JZ-GE) et Twin Turbo (2JZ-GTE) 1993', 4, 2),
(5, 2, 'nissan 350z', 15000, ' VQ35DE 6 cylindres en V3 3,5 litres 280 ch à 6 200 tr/min 2003', 5, 2),
(6, 1, 'nissan 370z', 32000, 'VQ37VHR 6 cylindres en V à 60°, 24 soupapes manuelle 6 vitesses 2008', 2, 1),
(7, 1, 'nissan GTR', 52000, 'GTR V6 de 3,8 litres bi-turbo, 485 ch à 6 400 tr/min 2010', 3, 1),
(8, 1, 'Honda S2000', 15000, 'S2000 4 cylindres en ligne DOHC V-TEC 16 soupapes de 2,0 litres (F20C) 2002 ', 10, 5),
(9, 2, 'honda accord V6', 6000, 'Accord coupe à 2 porte V6 avec 200ch. 2006', 2, 1),
(10, 1, 'Honda civic mugen', 20000, 'Mugen 4 portes 2.0 litre 4 cylindres K20Z3 i-VTEC 2010', 10, 5),
(11, 1, 'Nissan skyline R33', 15000, 'BCNR33 GT-R - 2.6 L RB26DETT DOHC twin-turbo six cylindres en ligne, 305 ch (224 kW)', 3, 1),
(12, 1, 'nissan skyline R34', 32000, 'GT-R - 2.6 L RB26DETT twin-turbo six cylindres en ligne, 331 ch (244 kW) (bridée à 280 ch car c''est la limite des voitures de particuliers au Japon)', 2, 1),
(13, 2, 'nissan 300ZX', 13000, 'Bi-turbo (VG30DETT) 1999, 5 vitesses, (220 ch)', 5, 2),
(14, 1, 'mitsubishi lancer Evolution X', 32000, ' 4-cylindres 16 soupapes turbo de 2 litres en aluminium 295 ch à 6 500 tr/min', 3, 2),
(15, 3, 'suzuki sx4', 999, '1.6 16V 2013 Suzuki Essence 79 Kw (107 Cv) 45 (Nm) ', 1, 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `clients` (`idClient`);

--
-- Contraintes pour la table `commandesdetails`
--
ALTER TABLE `commandesdetails`
  ADD CONSTRAINT `commandesdetails_ibfk_1` FOREIGN KEY (`idCommande`) REFERENCES `commandes` (`idCommande`),
  ADD CONSTRAINT `commandesdetails_ibfk_2` FOREIGN KEY (`idProduit`) REFERENCES `produits` (`idProduit`);

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `categories` (`idCategorie`);
