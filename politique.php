﻿<html>
<head>
	<meta charset='iso-8859-1'/>
	<link href="CSS/style.css" rel="stylesheet" type="text/css">
</head>
<?php
require_once "Biblio/FonctionCommune.php";
 //Affiche les standartds du site web.
    GrandeurFenetre();
	AfficheTitre();
	ColonneGauche();
	//Message des politiques de l'entreprise .
		echo "
		<td>
		<table border='0' width = '1000' height = '505'>
			<tr id='politique'><td>Nos politiques</td></tr>
			<tr id='politique'><td>Comme notre catalogue vous offre des automobiles usagées, notre inventaire varie grandement</td></tr>
			<tr id='politique'><td>Notre catalogue reflète l'état de notre inventaire dans la mesure du possible mais nous ne pouvons garantir la disponibilité de tous les automobiles.</td></tr>
			<tr id='politique'><td>Nous acceptons les cartes Visa, Mastercard et American Express et Paypal.</td></tr>
			<tr id='politique'><td>En raison de notre type de produits, et sauf dans les cas de problèmes majeurs, nous n'acceptons ni les retours ni les échanges.</td></tr>
			<tr id='politique'><td>Par contre, vous disposez de deux jours pour annuler toute commande. Passé ce délai, vos automoblies vous sont envoyés et le montant total de la transaction est affecté à votre carte de crédit ou compte PayPal.</td></tr>
			<tr id='politique'><td>Merci de votre clientèle</td></tr>
		</table>";
		
ColonneBas();
	
?>
</html>