﻿<html>
<head>
	<meta content="text/html; charset=iso-8859-1" />
	<link href="CSS/style.css" rel="stylesheet" type="text/css">
</head>
 <body>
<?php
	 require_once "Biblio/FonctionCommune.php";

	session_start();
	
	GrandeurFenetre();
	AfficheTitre();
	ColonneGauche();
	
	//Initialise un nouveau client.
	$client = new Client ($_SESSION['client']);
	$tClient = $client->getClient();
		
	if (isset($_SESSION['authentification']))
	{
		try { $BD = new BDService; } catch (Exception $e) { echo $e->getMessage(); }
		//Modifie les champs dans la BD.
		$SQLInsert = "update clients SET nom = '".$BD->Escape($tClient['nom'])."', prenom = '".$BD->Escape($tClient['prenom'])."' , numerociviqueetrue = '".$tClient['numerociviqueetrue']."' , ville = '".$tClient['ville']."', codepostal = '".$tClient['codepostal']."', province = '".$tClient['province']."', courriel = '".$tClient['courriel']."', 
		telephone = '".$tClient['telephone']."', genre = '".$tClient['genre']."' WHERE usager ='".$tClient['usager']."';";//Si l'usager est le même que dans la BD.
		
		try {$maj = $BD->UpdateDelete($SQLInsert);} //Essaye de faire le Update des champs.
		catch (Exception $e) {
			die ($e->getMessage());
		}
			if ($maj == 0)//Si rien n'a changé dans les champs. Affiche que rien n'a changé.
			{
				
			echo"<td colspan='2'><form action='authentification.php' method='post' >
				<table id='inscription' align='center'>
				  <tr>
					<td colspan='2'>
					</td>
				  </tr>
				  <tr>
					<td colspan='2'>Vous n’avez fait aucun changement</td> 
				  </tr>
			  </table>
			</form>
			</td>";
			}
			else{ //Sinon, affiche que le dossier a été modifié.
				echo"<td colspan='2'><form action='authentification.php' method='post' >
				<table id='inscription' align='center'>
				  <tr>
					<td colspan='2'>
					</td>
				  </tr>
				  <tr>
					<td colspan='2'>Votre dossier a été modifié avec succès</td>
				  </tr>
			  </table>
			</form>
			</td>";
			}
	}
	else //Si on n'est pas encore connecté avec un compte.
	{
		try { $BD = new BDService; }
		catch (Exception $e) { echo $e->getMessage(); }
		//Veut rentrer des informations du clients dans la BD.
		$SQLInsert = "insert into clients (nom, prenom, numerociviqueetrue, ville, codepostal, province, courriel, telephone, genre, usager, motPasse) values('".$BD->Escape($tClient['nom'])."',
				'".$BD->Escape($tClient['prenom'])."', '".$tClient['numerociviqueetrue']."', '".$tClient['ville']."', '".$tClient['codepostal']."', '".$tClient['province']."',
				'".$tClient['courriel']."', '".$tClient['telephone']."', '".$tClient['genre']."', '".$tClient['usager']."', md5('".$tClient['motPasse']."'))";
	
		try {
			$ajout=$BD->Insert($SQLInsert); //Essaye d'ajouté un client.
		}
		catch (Exception $e) {
		if ($e->getMessage() == 'doublon') { //Si le nom d'utilisateur existe déjà.
			header("location:inscription.php?message=doublon"); //Renvoit à l'inscription.
			exit();
		}
		else
			die ($e->getMessage());
	}
		//Affiche un message à l'utilisateur pour lui dire qu'il a réussit à créer un compte sur notre site.
		echo "<td id='inscription'> 
			<div id='reste'>
				<p>".$tClient['nom']." ".$tClient['prenom']."</p>
				<p>Votre dossier a été créé avec succès et dorénavant vous êtes inscrit sur notre site.</p>
				<p>À votre prochaine visite, il vous suffira de vous authentifier avec votre nom d'usager: ".$tClient['usager']." et votre mot de passe.</p>
				<p>Pour cette session, vous êtes déjà authentifié.</p>
				<p>Cliquez pour retourner au <a href='index.php'>catalogue</a></p>
			</div></td>";
		
		$_SESSION['authentification'] = $tClient['usager'];
	}
	
	$tClient->changerID($maj);
	
	ColonneBas();
?>
</body>
</html>