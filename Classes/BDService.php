<?php
final class BDService
{
// Attribut priv� qui deviendra une instance de la classe MySqli
private $BD_Interne;
// Constructeur pour instancier un nouvel objet de la classe BDService
public function __construct()
{
// Instanciation d'un nouvel objet, BD_Interne,de la classe mysqli 
$this->BD_Interne = new mysqli("420.cstj.qc.ca", "1051642", "inF123987", "1051642_tpauto");
if (mysqli_connect_errno()) 
// Quelque chose n'a pas bien march� on lance une exception 
throw new Exception ("Impossible de se connecter:" . mysqli_connect_error());
}
// M�thode de la classe BDService pour lire en BDet retourner un 
// tableau des rang�es obtenues
public function Select($sql)
{
$tableau = array();
$requete = $this->BD_Interne->query($sql);
if (!$requete) 
// Quelque chose n'a pas bien march� on lanceune exception 
throw new Exception ("Erreur SQL dans le SELECT: $sql (".$this->BD_Interne->error. ")"); 
// Tout va bien: on construit un tableau qui vacontenir toutes les rang�es obtenues par le select 
while ($ligneRequete = $requete->fetch_array(MYSQLI_ASSOC))
$tableau[]=$ligneRequete;
// On transmet le tableau de r�sultat � l'appelant
return $tableau; 
}

	public function Insert($ins)
	{
		$resultat = $this->BD_Interne->query($ins);
		if (!$resultat)
		{
			$message = 'doublon';
		if ($this->BD_Interne->errno !=1062)
			$message = "Erreur SQL dans le INSERT: $ins (".$this->BD_Interne->error. ")";
		throw new Exception ($message);
		}
	//Normalement, le nombre de lignes ins�r�es devrait �tre 1 
		$lignesAffectees = $this->BD_Interne->affected_rows;
		if ($lignesAffectees != 1)
		throw new Exception ('L\'insertion n\'a pas ajout� une ligne mais '.$lignesAffectees);
	//On retourne la valeur de la cl� qui vient d'�tre ins�r�e dans la BD
		return $this->BD_Interne->insert_id;
	} 
	
	public function UpdateDelete($sql)
	{  // UPDATE retourne 0 si la ligne est syntaxiquement correcte mais que l'information  // n'a pas chang� dans la BD. Il retourne 1 autrement (le nombre d'enregistrements affect�s)
	if (!stripos($sql, 'WHERE'))
		throw new Exception ('Danger!!! pas de WHERE dans l\'�nonc� UPDATE ou DELETE');
		$requete = $this->BD_Interne->query($sql);
	if (!$requete)
		throw new Exception ("Erreur SQL dans le DELETE ou UPDATE: $sql ". $this->BD_Interne->error());
	return $this->BD_Interne->affected_rows;
	} 

	public function Escape($info) {
		return $this->BD_Interne->real_escape_string($info);
	} 
	
}
?>