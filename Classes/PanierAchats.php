
<?php
class PanierAchats 
{
	private $panier;
	private $totalPanier;
	
	public function __construct() {
		$this->panier=array(); //On commence par un panier vide.
	}
		
	function afficherCommande()
	{
		if(isset($_SESSION['panier'][0])) //S'il y a au moins un premier item déjà dans le panier conservé.
		{
			$this->panier=$_SESSION['panier']; //on y met le contenu déjà mémorisé.
		}
			$nbAchats=count($this->panier);
		if ($nbAchats == 0)//S'il y a rien dans le panier on affiche :
		{
				echo"<td colspan='2'><form method='post' >
				<table id='inscription' align='center'>
				  <tr>
					<td colspan='2'>
					</td>
				  </tr>
				  <tr>
					<td colspan='2'>Votre commande est vide</td>
				  </tr>
				  <tr>
					<td>Pour retourner à la page d'accueil <a href='index.php'>cliquez-ici</a></td>
				  </tr>
			  </table>
			</form>
			</td>";
		}
		else {//Sinon on affiche les information avec les taxes du produit.
			echo "<td id='textePanier'><form method='post' action='panier.php?quoiFaire=modification'><table><div id='panierHaut'><br> Votre commande</br><div>";
			for($i=0; $i < $nbAchats; $i++) 
			{
			//Pour chaque achat.
				$article=$this->panier[$i];
				$article->afficherCom($i); 
				$this->totalPanier+=$article->total;
			}
			//Calcule les taxes.
				$tps=$this->totalPanier*TPS;
				$tvq=$this->totalPanier*TVQ;
				$livraison=2000;
				$grandTotal=$this->totalPanier+$tps+$tvq+$livraison;
			//Affiche les informations.
			
			echo "</td> 
				<tr id='taxe'><td colspan='5'></td><td>Total :</td><td>".number_format($this->totalPanier,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>TPS :</td><td>".number_format($tps,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>TVQ :</td><td>".number_format($tvq,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>Manutention et livraison :</td><td>".number_format($livraison,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>Grand total :</td><td>".number_format($grandTotal,2)." $</td></tr>
				<tr id='taxe'><td>Pour confirmer cette commande,<a href='carteCredit.php'>Cliquez ici</a></td></tr>
				</table>
				</form>";
		}
	}
	
	function afficherFacture()
	{
		$client = new client ($_SESSION['client']);
		
		$valeurClient = $client->getClient();
		
	
	
		if(isset($_SESSION['panier'][0])) //S'il y a au moins un premier item déjà dans le panier conservé.
		{
			$this->panier=$_SESSION['panier']; //on y met le contenu déjà mémorisé.
		}
			$nbAchats=count($this->panier);
	
			echo "<td id='textePanier'><form method='post' action='panier.php?quoiFaire=modification'><table><div id='panierHaut'><br> Facture </br><div>";
		if($valeurClient['genre']=='F')
		{
			echo "<tr id='infoClient'><td> F "; 
		}
		else
			echo "<tr id='infoClient'><td> M "; 
		
		echo $valeurClient['nom']," ", $valeurClient['prenom'], "</td></tr>";
		echo "<tr id='infoClient'><td>",$valeurClient['numerociviqueetrue'],"</tr></td>";
		echo "<tr id='infoClient'><td>",$valeurClient['ville']," (", $valeurClient['province'],")", "</tr></td>"; 
		echo "<tr id='infoClient'><td>",$valeurClient['codepostal'],"</td></tr><tr><td>' '</td></tr>";
		
			for($i=0; $i < $nbAchats; $i++) 
			{
			//Pour chaque achat.
				$article=$this->panier[$i];
				$article->afficherCom($i); 
				$this->totalPanier+=$article->total;
			}
			//Calcule les taxes.
				$tps=$this->totalPanier*TPS;
				$tvq=$this->totalPanier*TVQ;
				$livraison=2000;
				$grandTotal=$this->totalPanier+$tps+$tvq+$livraison;
			//Affiche les informations.
			echo "</td> ";
			
	
		
			echo" 
				<tr id='taxe'><td colspan='5'></td><td>Total :</td><td>".number_format($this->totalPanier,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>TPS :</td><td>".number_format($tps,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>TVQ :</td><td>".number_format($tvq,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>Manutention et livraison :</td><td>".number_format($livraison,2)." $</td></tr>
				<tr id='taxe'><td colspan='5'></td><td>Grand total :</td><td>".number_format($grandTotal,2)." $</td></tr>";
				
		
	}
	
	function afficher()
	{			
		if(isset($_SESSION['panier'][0])) //S�il y a au moins un premier item d�j� dans le panier conserv�.
		{
			$this->panier=$_SESSION['panier']; //on y met le contenu d�j� m�moris�.
		}
			$nbAchats=count($this->panier);
		if ($nbAchats == 0)//S'il y a rien dans le panier on affiche :
			echo "<td id='panierVide'>Votre panier est vide </td>";
		else {//Sinon on affiche les information avec les taxes du produit.
			echo "<td id='textePanier'><form method='post' action='panier.php?quoiFaire=modification'><table><div id='panierHaut'><br> Votre panier</br><div>";
			for($i=0; $i < $nbAchats; $i++) 
			{
			//Pour chaque achat.
				$article=$this->panier[$i];
				$article->afficher($i); 
				$this->totalPanier+=$article->total;
			}
			//Calcule les taxes.
				$tps=$this->totalPanier*TPS;
				$tvq=$this->totalPanier*TVQ;
				$livraison=2000;
				$grandTotal=$this->totalPanier+$tps+$tvq+$livraison;
			//Affiche les informations.
			echo "</td><td><a id='viderPanier' href=panier.php?quoiFaire=vider> vider le panier </a></td>
				<td>	
					<input id='modifierPanier' type='submit' value='Modifier les quantit�s'/>
				</td>
				<tr id='taxe'><td colspan='2'></td><td>Total :</td><td>".number_format($this->totalPanier,2)."</td></tr>
				<tr id='taxe'><td colspan='2'></td><td>TPS :</td><td>".number_format($tps,2)."</td></tr>
				<tr id='taxe'><td colspan='2'></td><td>TVQ :</td><td>".number_format($tvq,2)."</td></tr>
				<tr id='taxe'><td colspan='2'></td><td>Manutention et livraison :</td><td>$livraison</td></tr>
				<tr id='taxe'><td colspan='2'></td><td>Grand total :</td><td>".number_format($grandTotal,2)."</td></tr>
				</table>
				</form>";
		}
	 }
	 
	public function ajouter($noProduit) //Quand on ajoute un produit au panier.
	{	
	$this->panier= $_SESSION['panier'];
	
	$dejaPresent = false;//S'il existe d�j�
		$nbAchats = count($this->panier); //nb d�articles dans le panier
		if ($nbAchats > 0 )
		{
			for($i = 0; $i < $nbAchats; $i++)
				if ($noProduit == $this->panier[$i]->getNoProduit())
				{ //$noProduit est le num�ro � ajouter
					$dejaPresent = true; //pass� en param�tre � la m�thode
					break;
				}
		 }
		if ($dejaPresent) //Existe d�j�
		{
			$quantite = $this->panier[$i]->getQuantite(); //Lecture de la quantite actuelle
			$this->panier[$i]->setQuantite($quantite + 1);
		}
		else
		{
			try{$maBD = new BDService();}catch(exeption $e){	echo "$e->getmessage()";}
				
		
	$strSQL=  " SELECT * "; //Lire les informations de la BD.
	$strSQL.= " FROM produits, categories ";
	$strSQL.= " WHERE produits.idCategorie = categories.idCategorie AND produits.idProduit = ".$noProduit;
	
    try{$resultat = $maBD->Select($strSQL);}	catch(exeption $e)	{	echo "$e->getmessage()";}
		
		foreach($resultat as $produit)
		{//Ajoute un achat pour chaque produit.
			$achat = new Achat ($produit);
		}

		$this->panier[] = $achat;//Rajoute l'achat au panier.
		$_SESSION['panier'] = $this->panier;
	 }
  }


	 function modifier() {	//Modifier le nombre d'achat pour 1 produit.	
	 $this->panier= $_SESSION['panier'];
			$nbAchats = count($this->panier);
			$modele='/^[1-9]\d*$/';
			
			for($i=0; $i < $nbAchats; $i++) {
			//$this->panier= $_SESSION['panier'];
				if($_POST['quantite'.$i] != $this->panier[$i]->getQuantite()) {
					$this->panier[$i]->setQuantite($_POST['quantite'.$i]);
				}
				else {
					$_SESSION['panier'] = $this->panier;
				}
			}
		}
		
	   function supprimer($ligne) { //Supprimer un seul produit dans le panier.
			//Pour supprimer cet �l�ment du tableau, on l'�crase avec ceux qui suivent
			$this->panier= $_SESSION['panier'];
			$nbAchats = count($this->panier);
			
			for($i=$ligne+1; $i < $nbAchats; $i++)
				$this->panier[$i-1] = $this->panier[$i];

			unset ($this->panier[$nbAchats-1]);

			$_SESSION['panier'] = $this->panier;
		}
}//fin classe

?>