<?php
 class Catalogue
 {
	private $catalogue = array(); //Le catalogue est � null.
	
	private function ajouterProduit(Auto $produit) //On ajoute un produit dans le catalogue.
	{
		$this->catalogue[]=$produit;
	}
	
	public function __construct($categorie, $critere)//Constructeur du catalogue.
	{
	 //L�objet d�acc�s � la BD a �t� instanci� � l�ext�rieur de cette fonction
	// et le mot global le rend disponible dans la fonction
	
	$condition="";
	if($categorie !=0)//S'il y en n'a.
		$condition=" AND produits.idCategorie = ".$categorie;
	if($critere !="")
		$condition.=" AND (nom LIKE '%$critere%' OR description_produit LIKE '%$critere%')";
	
	global $maBD;
	$strSQL=  " SELECT * "; //Les informations de la BD.
	$strSQL.= " FROM produits, categories ";
	$strSQL.= " WHERE produits.idCategorie = categories.idCategorie".$condition;
	
	try {$resultat = $maBD->Select($strSQL);}catch(Exeption $e){	echo "$e->getmessage()";}
	
		foreach ($resultat as $ligne) //Pour chaque produit un l'ajoute dans la fonction du haut.
		{
			$produit = new Auto($ligne); 
			$this->ajouterProduit($produit);
		}
	}
	
	public function affiche()  //Envoi l'information pour qu'elle soit affich�.
	{
		$nbProduits = count($this->catalogue);
		if ($nbProduits == 0) //Si le catalogue n'a pas de produit en ce moment.
		{
			echo "Il n'y a pas de produit dans le catalogue.";
		}
		else {
		for ($i=0; $i < $nbProduits; $i++) //Si on n'a des produit on l'affiche avec l'aide de auto.php.
		{
			$this->catalogue[$i]->affiche(); //pour chaque produit du tableau, on fait appel � sa m�thode d�affichage
		}
			  }
	}
	
 }
 ?>