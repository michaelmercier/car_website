<?php
	class Client
	{
	private $idClient;
	private $nom;
	private $prenom;
	private $numerociviqueetrue;
	private $ville;
	private $codepostal;
	private $usager;
	private $motPasse;
	private $courriel;
	private $telephone;
	private $province;
	private $genre;
	
		public function __construct($tableau)
		{
			if($tableau== NULL)
			{
				foreach($this as $cle => $valeur)
				{
					$tableau[$cle]='';
				}	
			}
		
			$this->idClient = $tableau['idClient'];
			$this->nom = $tableau['nom'];
			$this->prenom = $tableau['prenom'];	
			$this->usager = $tableau['usager'];
			$this->motPasse = $tableau['motPasse'];
			$this->numerociviqueetrue = $tableau['numerociviqueetrue'];
			$this->ville = $tableau['ville'];
			$this->codepostal = $tableau['codepostal'];
			$this->province = $tableau['province'];
			$this->courriel = $tableau['courriel'];
			$this->telephone = $tableau['telephone'];
			$this->genre = $tableau['genre'];
		
			
		}
		
		function afficherNom()
		{
			echo "$this->nom";
		}
		
		function ObtenirMotPasse()
		{
			return $this->motPasse;
		}
		
		function changerValeur($mp)
		{
			$this->motPasse = $mp;
		}
		
		function changerID($np)
		{
			$this->idClient = $np;
		}
		
		public function getClient()
		{
			$tableau = array();
			$tableau["idClient"] = $this->idClient;
			$tableau["nom"] = $this->nom;
			$tableau["prenom"] = $this->prenom;	
			$tableau["usager"] = $this->usager;
			$tableau["motPasse"] = $this->motPasse;
			$tableau["numerociviqueetrue"] = $this->numerociviqueetrue;
			$tableau["ville"] = $this->ville;
			$tableau["codepostal"] = $this->codepostal;
			$tableau["province"] = $this->province;
			$tableau["courriel"] = $this->courriel;
			$tableau["telephone"] = $this->telephone;
			$tableau["genre"] = $this->genre;
			return $tableau;
		}
		
		function setClient($tableau)
		{
			$this->idClient = $tableau['idClient'];
			$this->nom = $tableau['nom'];
			$this->prenom = $tableau['prenom'];	
			$this->usager = $tableau['usager'];
			$this->motPasse = $tableau['motPasse'];
			$this->numerociviqueetrue = $tableau['numerociviqueetrue'];
			$this->ville = $tableau['ville'];
			$this->codepostal = $tableau['codepostal'];
			$this->province = $tableau['province'];
			$this->courriel = $tableau['courriel'];
			$this->telephone = $tableau['telephone'];
			$this->genre = $tableau['genre'];
		}
	
	}
?>