
<?php	
error_reporting(E_PARSE); 
 class Auto
 {
 //Les variables protected on peut avec en héritage.
	protected $idProduit;
	protected $idCategorie;
	protected $nom;
	protected $prix;
	protected $description_produit;
	protected $quantiteProduit;
	protected $quantiteminimal;
	protected $description_categorie;
	

	
 public function __construct($Tab) //Le construteur qui initialise les informations.
	{
			if($Tab== NULL)
			{
				foreach($this as $cle => $valeur)
				{
					$Tab[$cle]='';
				}	
			}
	
	$this->idProduit = $Tab["idProduit"];
	$this->idCategorie = $Tab["idCategorie"];
	$this->nom = $Tab["nom"];
	$this->prix = $Tab["prix"];
	$this->description_produit = $Tab["description_produit"];
	$this->quantiteProduit = $Tab["quantiteProduit"];
	$this->quantiteminimal = $Tab["quantiteminimal"];
	$this->description_categorie = $Tab["description_categorie"];
	}
	public function getProduit()
	{
		$Tab = array();
		$Tab["idProduit"] = $this->idProduit;
		$Tab["idCategorie"] = $this->idCategorie;
		$Tab["nom"] = $this->nom;	
		$Tab["prix"] = $this->prix;
		$Tab["description_produit"] = $this->description_produit;
		$Tab["quantiteProduit"] = $this->quantiteProduit;
		$Tab["quantiteminimal"] = $this->quantiteminimal;
	//	$Tab["description_categorie"] = $this->description_categorie;
		return $Tab;
	
	}
	function setProduit($Tab)
	{
			$this->idProduit = $Tab['idProduit'];
			$this->idCategorie = $Tab['idCategorie'];
			$this->nom = $Tab['nom'];	
			$this->prix = $Tab['prix'];
			$this->description_produit = $Tab['description_produit'];
			$this->quantiteProduit = $Tab['quantiteProduit'];
			$this->quantiteminimal = $Tab['quantiteminimal'];
	
	}
	
	function affiche() //Affiche les informations du catalogue.
	{
	$chemin="Images/";
	$nomImage=$chemin."pasImage.jpg";
	if(file_exists($chemin.$this->idProduit.".jpg"))//Pour afficher chaque image de chaque produit.
	$nomImage = $chemin.$this->idProduit.".jpg";
	
	echo "
	<tr id='catalogue'>
	  <td>
	    <a href='javascript:afficherDetails($this->idProduit)'>
		  <img src='".$nomImage. "'>
		 </a>
	  </td>";
//Affiche toutes l'informations.
	  echo "
		<td>$this->description_categorie</td>
		<td>$this->nom</td><td> $this->prix$</td>
		<td>$this->description_produit</td>
		<td>$this->quantiteProduit</td>";
		//Une image avec un panier pour envoyer l'information d'ajouter un produit au panier.
		echo "
		<td>
			<a href='panier.php?quoiFaire=ajout&noProduit=",$this->idProduit,"'>";
		echo "
			<img id='panier' src='Images/ajoutPanier.jpg' border='0'></a>
		</td>
	</tr>";
	
	}
	
	function afficherFenetreDetail() //Quand on clique sur la photo du catalogue. Ouvre une autre fenêtre et affiche : 
	{
		$chemin="Images/";
		$nomImage=$chemin."pasImage.jpg";
		if(file_exists($chemin.$this->idProduit.".jpg"))
		$nomImage = $chemin.$this->idProduit.".jpg";
		//Les information du produit avec l'image.
		echo "<img id='afficheImage' src='".$nomImage. "'> 
		<div id='texteFenetre'>
		<br>Categorie: $this->description_categorie
		<br>Nom: $this->nom 
		<br>Prix: $this->prix$ 
		<br>Description: $this->description_produit 
		<br>Nombre en inventaire: $this->quantiteProduit <br>\n
		</div>";
	}
	
 }
?>