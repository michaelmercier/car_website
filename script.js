function afficherDetails(idProduit) //Pour ouvrire une autre page internet avec les informations de mon produit èa vendre.
{
  url='filmsDetails.php?idProduit='+idProduit;
  win=window.open(url,'Details','resizable=yes,width=700,height=500,toolbar=no,menubar=no');
  win.focus(); 
} 

function premierChamp(NomForm) 
{
   var f=null;
   if (document.getElementById) {
       f=document.getElementById(NomForm)
   }
   else
       if (NomForm == 'changMP')
       {
         if (window.changMP) 
         {
             f=window.changMP;
         }
       }
   if (f)
       f.mpActuel.focus();
}
function premierChampMP() {
          var f=null;
          if (document.getElementById) {
              f=document.getElementById('changMP')
          }
          else {
              if (window.changMP)
                  f=window.changMP;
          }
          if (f)
               f.mpActuel.focus();
     }

/*---------------------------------------------
  Place le curseur dans le premier champ 
  du premier formulaire
----------------------------------------------*/  
function validerConfirm(champs) 
{
  for (i=0; i<3; i++)
  {
    if (champs[i].value.length == 0) 
    {
      alert ("Tous les champs sont requis");
      return false;
    }
  }
  if (champs[1].value != champs[2].value) 
  {
     alert ("Confirmation différente du nouveau mot de passe");
     return false;
  }
  if (champs[0].value == champs[1].value) 
  {
     alert ("Ancien et nouveau mots de passe identiques... opération inutile");
     return false;
  }
  if (champs[1].value.length < 5) 
  {
     alert("Le nouveau mot de passe doit comporter au moins 5 caractères");
     return false
  }
  return true;
}